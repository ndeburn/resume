var Mgr = {};

Mgr.newElem = (function (type, classname, innerHtml, id) {
    var elem = document.createElement(type);
    if ((typeof classname != typeof undefined) && (classname != '')) { elem.className = classname; }
    if ((typeof innerHtml != typeof undefined) && (innerHtml != '')) { elem.innerHTML = innerHtml; }
    if ((typeof id != typeof undefined) && (id != '')) { elem.setAttribute('id',id); }
    return elem;
});

Mgr.launch = (function () {
    Mgr.layout.createLayout();
    Mgr.personal.get();
    Mgr.exp.getJobs();
    Mgr.skills.getSkills();
});

Mgr.layout = {
    createLayout: (function(){
        this.createHeader();
        this.createInfoSec();
        this.createExpSec();
        this.createSkillSec();
    }),
    createHeader: (function () {
        var secTop = Mgr.newElem('section', 'row', '','secTop');
        var headTop = Mgr.newElem('header', '', '','headTop');
        var tmpSpacer = Mgr.newElem('div','tmpSpacer');

        $(secTop).append(headTop);
        $(secTop).append(tmpSpacer);

        $('#stage').append(secTop);
    }),
    createInfoSec: (function () {
        var secInfo = Mgr.newElem('section', 'row','','secInfo');
        var artPersonal = Mgr.newElem('article', 'col col-md-6','','artPersonal');
        var headPersonal = Mgr.newElem('header', 'cat', '<i class="fa fa-id-card"></i>PERSONAL DETAILS','headPersonal');
        var tblPersonal = Mgr.newElem('table','table tblInfo','','tblPersonal');
        
        $(artPersonal).append(headPersonal);
        $(artPersonal).append(tblPersonal);
        $(secInfo).append(artPersonal);
        
        var artContact = Mgr.newElem('article', 'col col-md-6','','artContact');
        var headContact = Mgr.newElem('header', 'cat', '<i class="fa fa-address-book"></i>CONTACT','headContact');
        var tblContact = Mgr.newElem('table','table tblInfo','','tblContact');

        $(artContact).append(headContact);
        $(artContact).append(tblContact);        
        $(secInfo).append(artContact);
        
        $('#stage').append(secInfo);

    }),
    createExpSec: (function () {
        var secExp = Mgr.newElem('section', 'row','','secExp');
        var artExp = Mgr.newElem('article', 'col col-md-12','','artExp');
        var headExp = Mgr.newElem('header', 'cat', '<i class="fa fa-briefcase"></i>EXPERIENCE','headExp');
        
        $(artExp).append(headExp);
        $(secExp).append(artExp);
        
        $('#stage').append(secExp);
    }),
    createSkillSec: (function () {
        var secSkills = Mgr.newElem('section', 'row','','secSkills');
        var artSkills = Mgr.newElem('article', 'col col-md-12','','artSkills');
        var headSkills = Mgr.newElem('header', 'cat', '<i class="fa fa-trophy"></i>MASTERED SKILLS','headSkills');

        $(artSkills).append(headSkills);
        $(secSkills).append(artSkills);
        
        $('#stage').append(secSkills);
    })
};

Mgr.personal = {
    data: null,
    url: "data/Personal.json",
    get: (function () {
        var myMgr = this;
        var ajaxsettings = {};
        ajaxsettings.url = Mgr.personal.url;
        ajaxsettings.method = "GET";
        ajaxsettings.dataType = "json";
        ajaxsettings.contentType = "application/json";
        ajaxsettings.cache = "false";
        ajaxsettings.success = (function (response, textStatus, jqXHR) {
            myMgr.data = response;
            myMgr.processDetails();
            myMgr.processContact();
        });
        ajaxsettings.fail = (function (jqXHR, textStatus, errorThrown) {
            error("Mgr.personal: ajax request - The following error occured: " + textStatus, errorThrown);
        });
        $.ajax(ajaxsettings);
    }),
    processDetails: (function () {
        for (var i = 0; i < this.data.details.length; i++) {
            this.renderRow(this.data.details[i],'#tblPersonal');
        }
    }),
    processContact: (function () {
        for (var i = 0; i < this.data.contact.length; i++) {
            this.renderRow(this.data.contact[i],'#tblContact');
        }
    }),
    renderRow: (function (d, objId) {
        var row = Mgr.newElem('tr','');
        var heading = Mgr.newElem('td', 'td-info info-title', d.heading);
        var icon = Mgr.newElem('td', 'td-info info-icon', '<i class="fa ' + d.icon + ' ' + d.color + '"></i>');
        var data = Mgr.newElem('td', 'td-info info-data', d.data);
        
        $(row).append(heading);
        $(row).append(icon);
        $(row).append(data);

        $(objId).append(row);
    })
};

Mgr.exp = {
    jobs: null,
    url: "data/Experience.json",
    getJobs: (function () {
        var myMgr = this;
        var ajaxsettings = {};
        ajaxsettings.url = Mgr.exp.url;
        ajaxsettings.method = "GET";
        ajaxsettings.dataType = "json";
        ajaxsettings.contentType = "application/json";
        ajaxsettings.cache = "false";
        ajaxsettings.success = (function (response, textStatus, jqXHR) {
            myMgr.jobs = response.jobs;
            myMgr.processJobs();
        });
        ajaxsettings.fail = (function (jqXHR, textStatus, errorThrown) {
            error("Mgr.exp: ajax request - The following error occured: " + textStatus, errorThrown);
        });
        $.ajax(ajaxsettings);
    }),
    processJobs: (function () {
        for (var i = 0; i < this.jobs.length; i++) {
            Mgr.exp.renderJob(this.jobs[i]);
        }
    }),
    renderJob: (function (j) {
        var comp = Mgr.newElem('div', 'col-md-3 job-company', j.company);
        var title = Mgr.newElem('div', 'col-md-9 job-title', j.title);
        var date = Mgr.newElem('div', 'col-md-3 job-date', j.startdate + ' - ' + j.enddate);
        var resp = Mgr.exp.responsibilities(j.responsibilities);

        var top = Mgr.newElem('div', 'row job-top');
        $(top).append(comp);
        $(top).append(title);

        var btm = Mgr.newElem('div', 'row job-bottom');
        $(btm).append(date);
        $(btm).append(resp);

        var myJob = Mgr.newElem('div', 'job-row');
        $(myJob).append(top);
        $(myJob).append(btm);

        $('#artExp').append(myJob);
    }),
    responsibilities: (function (r) {
        if (r.length > 0) {
            var lst = Mgr.newElem('ul', 'col-md-9 job-ul');
            for (var i = 0; i < r.length; i++) {
                var item = Mgr.newElem('li', 'job-li', r[i].line);
                $(lst).append(item);
            }
            return lst;
        }
    })
};
Mgr.skills = {
    skillsets: null,
    url: "data/Skills.json",
    getSkills: (function () {
        var myMgr = this;
        var ajaxsettings = {};
        ajaxsettings.url = Mgr.skills.url;
        ajaxsettings.method = "GET";
        ajaxsettings.dataType = "json";
        ajaxsettings.contentType = "application/json";
        ajaxsettings.cache = "false";
        ajaxsettings.success = (function (response, textStatus, jqXHR) {
            myMgr.skillsets = response.skillsets;
            myMgr.processSkillsets();
        });
        ajaxsettings.fail = (function (jqXHR, textStatus, errorThrown) {
            error("skillMgr: ajax request - The following error occured: " + textStatus, errorThrown);
        });
        $.ajax(ajaxsettings);
    }),
    processSkillsets: (function () {
        var myRow = Mgr.newElem('div', "row row-skillsets");
        for (var i = 0; i < this.skillsets.length; i++) {
            var mySkillset = this.renderSkillset(this.skillsets[i], myRow);
            $(myRow).append(mySkillset);
            if ((i == 3 || i == 7 || i == 11) && (i != this.skillsets.length-1)) {
                $("#artSkills").append(myRow);
                myRow = Mgr.newElem('div', "row row-skillsets");
            }
        }
        $("#artSkills").append(myRow);

    }),
    renderSkillset: (function (s,rowElem) {
        var art = Mgr.newElem('div', 'col-md-3 art-skillset');
        var header = Mgr.newElem('header', 'skillset-header');

        $(header).append('<i class="fa ' + s.icon + ' skillset-icon"></i>');
        $(header).append(s.skillset);

        var skills = this.skillList(s.skills);

        $(art).append(header);
        $(art).append(skills);
        
        return art;
    }),
    
    skillList: (function (s) {
        if (s.length > 0) {
            var lst = Mgr.newElem('ul', 'skill-ul');
            for (var i = 0; i < s.length; i++) {
                var item = Mgr.newElem('li', 'skill-li', s[i].skill);
                $(lst).append(item);
            }
            return lst;
        }
    })
};

$(document).ready(function () {
    Mgr.launch();
});

